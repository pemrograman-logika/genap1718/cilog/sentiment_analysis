

negate_score(CurrentScore,UpdatedScore) :- UpdatedScore is -1 * CurrentScore.

dict_atom(X,CurrentScore, UpdatedScore) :- incrementor(X, IncrementorScore), UpdatedScore is IncrementorScore * CurrentScore.
dict_atom(X,CurrentScore, UpdatedScore) :- negator(X), negate_score(CurrentScore, UpdatedScore).
dict_atom(X,CurrentScore, UpdatedScore) :- score_atom(X,AtomScore), UpdatedScore is CurrentScore + AtomScore.

proceed([],ScoreNow, FinalScore) :- FinalScore is ScoreNow.

proceed([H|T],ScoreNow,FinalScore) :- atom_string(Atom, H), atom(Atom),
                                      dict_atom(Atom,ScoreNow, UpdatedScore),
                                      proceed(T, UpdatedScore, FinalScore), !.

proceed([_|T],ScoreNow, FinalScore) :- proceed(T,ScoreNow, FinalScore).

get_score(Input,FinalScore) :-string_lower(Input,Lowercase),
                              re_replace("[^A-Za-z]+", " ", Lowercase, Word),
                              split_string(Word,"\s\n\t-_"," ",List),
                              reverse(List,RevList),
			      length(List,LenScore),
                              proceed(RevList,0,SentenceScore),
			      FinalScore is SentenceScore / LenScore.

process_paragraph(Paragraph, FinalScore) :- split_string(Paragraph, ".!?", " ", Sentences),
                                           get_paragraph_score(Sentences, 0, FinalScore).

get_paragraph_score([], Score, FinalScore) :- FinalScore is Score.
get_paragraph_score([H|T], Score, FinalScore) :- get_score(H, SentenceScore),
                                                 Y is Score + SentenceScore,
                                                 get_paragraph_score(T, Y, FinalScore).
