#!/usr/bin/swipl -f -q

:- set_prolog_flag(verbose, silent).

:- initialization main.

:-consult(dict).
:-consult(dict_negator).
:-consult(dict_incrementor).
:-consult(utils).

main :-
    current_prolog_flag(argv, [H|[]]),
    atom_string(H,Str),
    process_paragraph(Str,Score),
    format('~q',Score),
    halt.
