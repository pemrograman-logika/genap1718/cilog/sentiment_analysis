# VIM Sentiment Analyzer

VIM Sentiment Analyzer is a LINE Bot application for sentiment analysis in microblog (short text). VIM Sentiment Analyzer uses
Prolog to process and score the text, and Python to communicate with LINE Bot as the user interface.

## How to Use (for Demo Purposes)

1. Add our LINE Bot Account @bxc8683h or: ![](qrcode.jpg)
2. Text the bot your short review (in BAHASA).
3. The bot will response the sentiment in floating point number format.


## Installation

1. Download / clone this project
2. Install requirements.txt
3. Obtain CHANNEL_ACCESS_TOKEN and CHANNEL_SECRET from your LINE Developer Account
4. Add CHANNEL_ACCESS_TOKEN and CHANNEL_SECRET to environment variables
5. Run application by running app.py (python app.py)

## Support

Please open an issue to receive support for this project.

## Contributors
[Muhammad](https://github.com/M46F/) - 1506735641

[Nur Intan](https://github.com/nurintaaan) - 1506689093

[Valentina](https://github.com/valentinakania/) - 1506757264

## Contributing

Fork the project, create a new branch, make your changes, and open a pull request.
